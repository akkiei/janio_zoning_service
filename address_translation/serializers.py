from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import as_serializer_error
from django.core.exceptions import ValidationError as DjangoValidationError
from location.serializers import LocationSerializer


class AddressTranslationRequestSerializer(serializers.Serializer):

	city = serializers.CharField(max_length=100, allow_null=False, allow_blank=False)
	state = serializers.CharField(max_length=100, allow_null=False, allow_blank=False)
	country = serializers.CharField(max_length=100, allow_null=False, allow_blank=False)
	postal_code = serializers.CharField(max_length=100, allow_blank=True)

	def validate(self, attrs):
		city = attrs.get('city', None)
		state = attrs.get('state', None)
		country = attrs.get('country', None)

		if country is None or state is None or city is None:
			raise ValidationError("country/state/city can't be None or empty.")

		return attrs


