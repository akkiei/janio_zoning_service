from django.core.validators import MinValueValidator, MaxValueValidator, validate_comma_separated_integer_list
from django.db import models
from location.models import City, State, Country


class AddressTranslation(models.Model):
	created_at = models.DateField(auto_now_add=True, null=False)
	updated_at = models.DateField(auto_now_add=True, null=False)
	is_active = models.BooleanField(default=True)
	is_success = models.BooleanField(default=True)
	city_id = models.ForeignKey(City, to_field='city_id', on_delete=models.CASCADE)
	state_id = models.ForeignKey(State, to_field='state_id', on_delete=models.CASCADE)
	country_id = models.ForeignKey(Country, to_field='country_id', on_delete=models.CASCADE)
	postal_prefix = models.CharField(validators=[validate_comma_separated_integer_list], max_length=200, blank=True, null=True, default='')
	city_score = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)], default=0)
	state_score = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)], default=0)
	country_score = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)], default=0)
	overall_score = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(1)], default=0)

	# def __str__(self):
	# 	return "".format()
	# class Meta:
	# 	constraints = [
	# 		models.UniqueConstraint(fields=['city_id', 'country_id', 'state_id'], name='unique')
	# 	]


class AddressTranslationLogs(models.Model):
	address_translation = models.ForeignKey(AddressTranslation, on_delete=models.CASCADE)
	created_at = models.DateField(auto_now_add=True, null=False)
	updated_at = models.DateField(auto_now_add=True, null=False)
	raw_address = models.TextField()
