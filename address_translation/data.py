

COUNTRY_CITY_LEVEL_POSTAL_PREFIX_DIGITS = {
  'Australia': 2,
  'Canada': 3,
  'China': 4,
  'Taiwan': 3,
  'Thailand': 3,
  'US': 3,
  'Vietnam': 2,
  'Indonesia': 3,
  'Malaysia': 3,
  'Philippines': 3,
  'South Korea': 3
}

COUNTRY_STATE_LEVEL_POSTAL_PREFIX_DIGITS = {
  'Thailand': 2,
  'Indonesia': 2,
  'Malaysia': 2,
  'Philippines': 2
}