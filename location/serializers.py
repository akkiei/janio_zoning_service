from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from partner.models import Partner
from zone.models import Zone
from .models import City, State, Country, Location


class LocationSerializer(serializers.Serializer):
	city_id = serializers.IntegerField(allow_null=False)
	state_id = serializers.IntegerField(allow_null=False)
	country_id = serializers.IntegerField(allow_null=False)

	def validate(self, data):
		city = City.objects.filter(id=data.get('city_id', None)).first()
		state = State.objects.filter(id=data.get('state_id', None)).first()
		country = Country.objects.filter(id=data.get('country_id', None)).first()
		if city and state and country:
			data['city_id'] = city
			data['state_id'] = state
			data['country_id'] = country
			return data
		else:
			raise ValidationError("City/State/Country ID is None")

	def get_location_id(self):
		with transaction.atomic():
			max_id = Location.objects.filter().order_by('-location_id').first()
			return max_id.zone_id + 1

	def save(self, validated_data):
		Location.objects.create(**validated_data)

	def create(self, validated_data):
		partner_instance = Partner.objects.filter(id=validated_data.get('partner', None)).first()
		if partner_instance:
			validated_data['partner'] = partner_instance
			Zone.objects.create(**validated_data)
		else:
			raise serializers.ValidationError("Partner id is invalid")


# class StateSerializer(serializers.Serializer):

