from django.db import models, transaction
from rest_framework.exceptions import ValidationError


class Country(models.Model):
	country_id = models.IntegerField(unique=True)
	country_name = models.CharField(max_length=100, null=False, blank=False, unique=True)
	created_at = models.DateField(auto_now_add=True, null=False)

	class Meta:
		verbose_name = 'Country'
		verbose_name_plural = 'Countries'

	def __str__(self):
		return self.country_name

	def get_country_id(self):
		with transaction.atomic():
			max_id = Country.objects.filter().order_by('-country_id').first()
			return max_id.country_id + 1

	def save(self, *args, **kwargs):
		self.country_id = self.get_country_id()
		super(Country, self).save(*args, **kwargs)


class State(models.Model):
	state_id = models.IntegerField(unique=True)
	state_name = models.CharField(max_length=100, null=False, blank=False)
	country = models.ForeignKey(Country, db_column='country_id', on_delete=models.CASCADE)
	created_at = models.DateField(auto_now_add=True, null=False)

	def __str__(self):
		return self.state_name

	def get_state_id(self):
		with transaction.atomic():
			max_id = State.objects.filter().order_by('-state_id').first()
			return max_id.state_id + 1

	def save(self, *args, **kwargs):
		self.state_id = self.get_state_id()
		super(State, self).save(*args, **kwargs)


class City(models.Model):
	city_id = models.IntegerField(unique=True)
	city_name = models.CharField(max_length=100, null=False, blank=False)
	state = models.ForeignKey(State, db_column='state_id', on_delete=models.CASCADE)
	postal_prefix = models.CharField(max_length=100, null=True)
	postal_prefix_len = models.IntegerField(null=True)
	created_at = models.DateField(auto_now_add=True, null=False)

	class Meta:
		verbose_name = 'City'
		verbose_name_plural = 'Cities'

	def __str__(self):
		return self.city_name

	def get_city_id(self):
		with transaction.atomic():
			max_id = City.objects.filter().order_by('-city_id').first()
			return max_id.city_id + 1

	def save(self, *args, **kwargs):
		self.city_id = self.get_city_id()
		super(City, self).save(*args, **kwargs)


class Location(models.Model):
	location_id = models.IntegerField(unique=True, blank=True, null=True)
	city = models.ForeignKey(City, db_column='city_id', on_delete=models.CASCADE, null=True)
	state = models.ForeignKey(State, db_column='state_id', on_delete=models.CASCADE, null=True)
	country = models.ForeignKey(Country, db_column='country_id', on_delete=models.CASCADE, null=True)
	created_at = models.DateField(auto_now_add=True, null=False)
	updated_at = models.DateField(auto_now_add=True, null=False)

	def get_location_id(self):
		with transaction.atomic():
			max_id = Location.objects.filter().order_by('-location_id').first()
			return max_id.location_id + 1

	def save(self, *args, **kwargs):
		self.location_id = self.get_location_id()
		super(Location, self).save(*args, **kwargs)

	def __str__(self):
		return str(self.location_id)
