from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
	@classmethod
	def get_token(cls, user):
		token = super().get_token(user)
		# Add custom claims
		# token['partner_id'] = user.id
		token['service'] = 'janio_zoning'

		return token
