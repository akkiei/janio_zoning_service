# Generated by Django 3.2.13 on 2022-07-01 06:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0013_auto_20220701_0558'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='public_key',
            field=models.CharField(default='a7d431b7ed804a64805190e8f2ae4dae', max_length=100, unique=True),
        ),
    ]
