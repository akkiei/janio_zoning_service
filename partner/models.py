import uuid

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import PermissionsMixin


class PartnerManager(BaseUserManager):

	def create_user(self, name, password):
		if not name or not password:
			raise ValueError("Name/password is required.")

		name = name.lower()
		user = self.model(name=name)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, name, password):
		"""Creates and saves a new superuser with given details."""
		user = self.create_user(name, password)
		user.is_superuser = True
		user.is_staff = True
		user.save(using=self._db)
		return user


class Partner(AbstractBaseUser, PermissionsMixin):
	username = models.CharField(max_length=100, null=False, blank=False, unique=True)
	created_at = models.DateField(auto_now_add=True, null=False)
	updated_at = models.DateField(auto_now_add=True, null=False)
	password = models.CharField(max_length=200, null=False, blank=False)
	is_active = models.BooleanField(default=True)
	is_superuser = models.BooleanField(default=False)
	is_staff = models.BooleanField(default=True)
	disable_zones = models.BooleanField(default=False)
	public_key = models.CharField(max_length=100, default=uuid.uuid4().hex, unique=True, null=False)
	min_threshold = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)], default=80)
	max_threshold = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)], default=95)

	objects = PartnerManager()
	USERNAME_FIELD = 'username'

	def __str__(self):
		return self.username

	def get_full_name(self):
		"""Used to get a users full name."""

		return self.username
