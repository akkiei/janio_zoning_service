from datetime import datetime, timedelta

from django.core.cache import cache
from django.db import models
from django.utils import timezone
from django.utils.module_loading import import_string
from djmoney.settings import (
    CURRENCY_CODE_MAX_LENGTH,
    EXCHANGE_BACKEND,
    RATES_CACHE_TIMEOUT,
)

from .exceptions import MissingBackend, MissingRate


class ExchangeBackend(models.Model):
    name = models.CharField(max_length=255)
    rate_card_name = models.CharField(max_length=255)
    base_currency = models.CharField(max_length=CURRENCY_CODE_MAX_LENGTH)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return ": ".join([self.name, self.rate_card_name])

    def clear_rates(self):
        self.rates.all().delete()

    class Meta:
        get_latest_by = "last_updated"
        unique_together = (("name", "rate_card_name"),)


class Rate(models.Model):
    currency = models.CharField(max_length=CURRENCY_CODE_MAX_LENGTH)
    value = models.DecimalField(max_digits=20, decimal_places=6)
    backend = models.ForeignKey(
        ExchangeBackend, on_delete=models.CASCADE, related_name="rates"
    )

    class Meta:
        unique_together = (("currency", "backend"),)


def get_default_backend_name():
    return import_string(EXCHANGE_BACKEND).name


def get_rate(source, target, backend_name=None, rate_card_name=None, date=None):
    """
    Returns an exchange rate between source and target currencies.
    Converts exchange rate on the DB side if there is no backends with given base currency.
    Uses data from the default backend if the backend is not specified.
    """
    if backend_name is None:
        backend_name = get_default_backend_name()

    if date:
        key = (
            f"djmoney:get_rate:{source}:{target}:{backend_name}:{rate_card_name}:{date}"
        )
        date = datetime.strptime(date, "%Y-%m-%d") + timedelta(days=1)
        date = date.replace(tzinfo=timezone.utc)
    if rate_card_name:
        key = f"djmoney:get_rate:{source}:{target}:{backend_name}:{rate_card_name}"
    else:
        key = f"djmoney:get_rate:{source}:{target}:{backend_name}"

    result = cache.get(key)
    if result is not None:
        return result

    backend_objs = ExchangeBackend.objects.filter(name=backend_name)
    if not backend_objs:
        raise MissingBackend(f"Exchange backend {backend_name} does not exist")

    backend_objs = (
        backend_objs.filter(rate_card_name=rate_card_name)
        if rate_card_name
        else backend_objs
    )
    backend_objs = backend_objs.filter(last_updated__lt=date) if date else backend_objs
    backend = backend_objs.latest()
    if not backend_objs or date and date - backend.last_updated > timedelta(days=7):
        raise MissingBackend("Exchange rates do not exist for this date")

    result = _get_rate(source, target, backend)
    cache.set(key, result, RATES_CACHE_TIMEOUT)
    return result


def _get_rate(source, target, backend):
    source, target = str(source), str(target)
    if str(source) == target:
        return 1
    rates = Rate.objects.filter(
        currency__in=(source, target), backend=backend
    ).select_related("backend")
    if not rates:
        raise MissingRate(f"Rate {source} -> {target} does not exist")
    if len(rates) == 1:
        return _try_to_get_rate_directly(source, target, rates[0])
    return _get_rate_via_base(rates, target)


def _try_to_get_rate_directly(source, target, rate):
    """
    Either target or source equals to base currency of existing rate.
    """
    # Converting from base currency to target
    if rate.backend.base_currency == source and rate.currency == target:
        return rate.value
    # Converting from target currency to base
    elif rate.backend.base_currency == target and rate.currency == source:
        return 1 / rate.value
    # Case when target or source is not a base currency
    raise MissingRate(f"Rate {source} -> {target} does not exist")


def _get_rate_via_base(rates, target):
    """
    :param: rates: A set/tuple of two base Rate instances
    :param: target: A string instance of the currency to convert to

    Both target and source are not a base currency - actual rate could be calculated via their rates to base currency.
    For example:

    7.84 NOK = 1 USD = 8.37 SEK

    7.84 NOK = 8.37 SEK

    1 NOK = 8.37 / 7.84 SEK
    """
    first, second = rates
    # Instead of expecting an explicit order in the `rates` iterable, that will put the
    # source currency in the first place, we decided to add an extra check here and swap
    # items if they are ordered not as expected
    if first.currency == target:
        first, second = second, first
    return second.value / first.value


def convert_money(value, currency, backend_name=None, rate_card_name=None, date=None):

    amount = value.amount * get_rate(
        value.currency, currency, backend_name, rate_card_name, date
    )
    return value.__class__(amount, currency)
