class MissingRate(Exception):
    """
    Absence of some exchange rate.
    """


class MissingBackend(Exception):
    """
    Absence of some exchange backend.
    """
