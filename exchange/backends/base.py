from urllib.parse import parse_qsl, urlencode, urlparse, urlunparse

import requests
from django.db.transaction import atomic
from djmoney import settings

from exchange.models import ExchangeBackend, Rate


class BaseExchangeBackend:
    name = None

    def __init__(
        self,
    ):
        raise NotImplementedError

    def get_params(self):
        return {}

    def get_url(self, **params):
        """
        Updates base url with provided GET parameters.
        """
        parts = list(urlparse(self.url))
        query = dict(parse_qsl(parts[4]))
        query.update(params)
        parts[4] = urlencode(query)
        return urlunparse(parts)

    @atomic
    def update_rates(self, base_currency=settings.BASE_CURRENCY, **kwargs):
        """
        Updates rates for the given backend.
        """
        backend, _ = ExchangeBackend.objects.update_or_create(
            name=self.name,
            rate_card_name=self.rate_card_name,
            defaults={"base_currency": base_currency},
        )

        backend.clear_rates()

        params = self.get_params()
        Rate.objects.bulk_create(
            [
                Rate(currency=currency, value=value, backend=backend)
                for currency, value in self.get_rates(**params).items()
            ]
        )

    def get_rates(self, **params):
        url = self.get_url(**params)
        response = requests.get(url)
        return response.json()["rates"]
