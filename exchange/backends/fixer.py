from datetime import datetime

from django.core.exceptions import ImproperlyConfigured
from django.utils import timezone
from djmoney import settings

from exchange.backends.base import BaseExchangeBackend


class FixerBackend(BaseExchangeBackend):
    name = "fixer.io"

    def __init__(self, url=settings.FIXER_URL, access_key=settings.FIXER_ACCESS_KEY):
        self.url = url
        self.access_key = access_key

        today = datetime.now(tz=timezone.utc).strftime("%Y_%m_%d")
        self.rate_card_name = "_".join(["daily", today])
        self.rate_card_name = today

        if self.access_key is None:
            raise ImproperlyConfigured(
                "settings.FIXER_ACCESS_KEY should be set to use FixerBackend"
            )

    def get_params(self):
        return {"apikey": self.access_key, "base": settings.BASE_CURRENCY}
