from moneyed import CurrencyDoesNotExist
from rest_framework import status, viewsets
from rest_framework.response import Response

from exchange.backends.janio import JanioBackend
from exchange.exceptions import MissingBackend, MissingRate
from exchange.serializers import ConvertRateSerializer, RateCardSerializer


class RateCardViewset(viewsets.ViewSet):
    def create(self, request):
        serializer = RateCardSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        rates = {rate.get("currency"): rate.get("value") for rate in data.get("rates")}
        JanioBackend(data.get("rate_card_name"), rates=rates).update_rates()
        return Response(
            {"message": "Successfully updated rates from %s" % JanioBackend.name},
            status=status.HTTP_200_OK,
        )


class ConvertRateViewset(viewsets.ViewSet):
    def create(self, request):
        from djmoney.money import Money

        from exchange.models import convert_money

        serializer = ConvertRateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.validated_data
        base_currency = validated_data.get("base_currency")
        target_currency = validated_data.get("target_currency")
        base_currency_value = validated_data.get("base_currency_value")
        backend_name = validated_data.get("backend_name")
        rate_card_name = validated_data.get("rate_card_name")
        date = validated_data.get("date")

        try:
            target_currency_value = convert_money(
                value=Money(base_currency_value, base_currency),
                currency=target_currency,
                backend_name=backend_name,
                rate_card_name=rate_card_name,
                date=date,
            )
        except (MissingRate, MissingBackend, CurrencyDoesNotExist) as e:
            return Response(
                {"message": e.args},
                status=status.HTTP_400_BAD_REQUEST,
            )

        response_data = {
            **validated_data,
            "target_currency_value": target_currency_value.amount,
        }
        response_data["base_currency_value"] = float(base_currency_value)

        return Response(
            response_data,
            status=status.HTTP_200_OK,
        )
