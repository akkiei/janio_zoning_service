from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import as_serializer_error
from django.core.exceptions import ValidationError as DjangoValidationError

from .models import Zone
from partner.models import Partner
from location.serializers import LocationSerializer
from datetime import datetime
from location.models import Location


class ZoneSerializer(serializers.Serializer):
	# child = LocationSerializer(many=True)
	name = serializers.CharField(max_length=100, allow_null=False)
	partner = serializers.IntegerField(allow_null=False)
	start_date = serializers.DateTimeField(allow_null=False)
	end_date = serializers.DateTimeField(allow_null=False)
	locations = LocationSerializer(allow_null=False, many=True)

	def validate(self, data):
		start_date = data.get('start_date', None)
		start_dt_tm = start_date.replace(tzinfo=None) if isinstance(start_date, datetime) else None
		end_date = data.get('end_date', None)
		end_dt_tm = end_date.replace(tzinfo=None) if isinstance(end_date, datetime) else None
		now = datetime.now(tz=None)
		if start_dt_tm is None or end_dt_tm is None:
			raise ValidationError("Incorrect/Empty date")
		try:
			if start_dt_tm < now < end_dt_tm:
				return data
		except Exception as err:
			raise serializers.ValidationError(err)

	# "start date/time can't be after or equal to end date")

	def save(self, validated_data):
		partner_instance = Partner.objects.filter(id=validated_data.get('partner', None)).first()
		if partner_instance:
			validated_data['partner'] = partner_instance
			zone_id = self.get_zone_id()
			validated_data['zone_id'] = zone_id
			Zone.objects.create(**validated_data)
		else:
			raise serializers.ValidationError("Partner id is invalid")

	def create(self, validated_data):
		partner_instance = Partner.objects.filter(id=validated_data.get('partner', None)).first()
		locations = validated_data['locations']
		location_instance = []
		if partner_instance:
			validated_data['partner'] = partner_instance
			with transaction.atomic():
				zone_instance = Zone(name=validated_data.get('name'), partner=validated_data.get('partner'),
				                     start_date=validated_data.get('start_date'),
				                     end_date=validated_data.get('end_date'))
				zone_instance.save()
				for loc in locations:
					loc_instance = Location.objects.filter(city_id=loc.get('city_id'),
					                                       state_id=loc.get('state_id'),
					                                       country_id=loc.get('country_id')).first()
					if loc_instance is None:
						loc_instance = Location(city_id=loc.get('city_id'), state_id=loc.get('state_id'),
						                        country_id=loc.get('country_id'))
						loc_instance.save()
					location_instance.append(loc_instance)
				zone_instance.locations.set(location_instance)
			return zone_instance


# else:
# 	raise serializers.ValidationError("Partner id is invalid")


class ZoneUpdateSerializer(serializers.Serializer):
	pk = serializers.IntegerField(allow_null=False)
	partner = serializers.IntegerField(allow_null=False)
	name = serializers.CharField(max_length=100, allow_null=False)
	start_date = serializers.DateTimeField(allow_null=False)
	end_date = serializers.DateTimeField(allow_null=False)

	def validate(self, attrs):
		partner = attrs.get('partner')
		pk = attrs.get('pk')

		zone_instance = Zone.objects.filter(zone_id=pk, partner=partner).first()
		if zone_instance is None:
			raise ValidationError("zone id doesn't exist or You don't have access to modify this zone.")
		start_date = attrs.get('start_date', None)
		start_dt_tm = start_date.replace(tzinfo=None) if isinstance(start_date, datetime) else None
		end_date = attrs.get('end_date', None)
		end_dt_tm = end_date.replace(tzinfo=None) if isinstance(end_date, datetime) else None
		now = datetime.now(tz=None)
		if start_dt_tm is None or end_dt_tm is None:
			raise ValidationError("Incorrect/Empty date")
		try:
			if start_dt_tm < now < end_dt_tm:
				attrs['instance'] = zone_instance
				return attrs
		except Exception as err:
			raise serializers.ValidationError(err)

	def update(self, instance, validated_data):
		instance.name = validated_data.get('name', instance.name)
		instance.start_date = validated_data.get('start_date', instance.start_date)
		instance.end_date = validated_data.get('end_date', instance.end_date)
		instance.update()
		return instance


class ZoneLocationUpdateSerializer(serializers.Serializer):
	partner = serializers.IntegerField(allow_null=False)
	zone_id = serializers.IntegerField(allow_null=False)
	locations = LocationSerializer(allow_null=False, many=True)

	def validate(self, attrs):
		zone_id = attrs.get('zone_id', None)
		partner = attrs.get('partner', None)

		if zone_id and partner:
			instance = Zone.objects.filter(zone_id=zone_id, partner=partner).first()
			if instance:
				attrs['instance'] = instance
				return attrs
		raise ValidationError("zone_id is None/empty or invalid")

	def update(self, instance, validated_data):
		locations = validated_data.get('locations', instance.locations)
		location_instances = []
		for loc in locations:
			loc_instance = Location.objects.filter(city_id=loc.get('city_id'),
			                                       state_id=loc.get('state_id'),
			                                       country_id=loc.get('country_id')).first()
			if loc_instance is None:
				loc_instance = Location(city_id=loc.get('city_id'), state_id=loc.get('state_id'),
				                        country_id=loc.get('country_id'))
				loc_instance.save()
			location_instances.append(loc_instance)
		instance.locations.set(location_instances)
		instance.update()
		return instance
