from django.db import IntegrityError
from rest_framework.exceptions import ValidationError
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .serializers import ZoneSerializer, ZoneUpdateSerializer, ZoneLocationUpdateSerializer

from zone.models import Zone


class ZoneViewSet(viewsets.ViewSet):

	def get_permissions(self):
		permission_classes = [IsAuthenticated]
		return [permission() for permission in permission_classes]

	def list(self, request):
		partner_id = request.auth['partner_id']
		if partner_id is not None:
			query_set = Zone.objects.filter(partner=partner_id).all().values('zone_id', 'name', 'is_active',
			                                                                 'partner_id',
			                                                                 'start_date', 'end_date')
			return Response({"zones": list(query_set)}, status=status.HTTP_200_OK)
		else:
			return Response({"error": "partner id is incorrect"}, status=status.HTTP_400_BAD_REQUEST)

	def create(self, request):
		request_data = request.data
		partner_id = request.auth['partner_id']
		temp =[req.update(partner=partner_id) for req in request_data]

		serializer = ZoneSerializer(data=request_data, many=True)
		serializer.is_valid(raise_exception=True)
		try:
			validate_data = serializer.validated_data
			response = serializer.create(validate_data)
			response_data = [{'zone_id': res.zone_id, 'name': res.name} for res in response]
			return Response(response_data, status=status.HTTP_201_CREATED)
		except (ValidationError, IntegrityError) as err:
			return Response({"error": str(err)}, status=status.HTTP_400_BAD_REQUEST)

	# # only update zone-location relation
	# def partial_update(self, request, pk=None):
	# 	req_data = request.data

	# only update zone specific fields
	def update(self, request, pk=None):
		req_data = request.data
		req_data['partner'] = request.auth['partner_id']
		req_data['pk'] = pk
		serializer = ZoneUpdateSerializer(data=req_data)
		serializer.is_valid(raise_exception=True)
		try:
			validated_data = serializer.validated_data
			response = serializer.update(validated_data=validated_data, instance=validated_data['instance'])
			return Response({
				"message": "id {} updated successfully!".format(pk)
			}, status=status.HTTP_200_OK)
		except (ValidationError, IntegrityError) as err:
			return Response({"error": str(err)}, status=status.HTTP_400_BAD_REQUEST)

	def destroy(self, request):
		pass


class ZoneLocationViewSet(viewsets.ViewSet):

	def get_permissions(self):
		permission_classes = [IsAuthenticated]
		return [permission() for permission in permission_classes]

	def list(self, request):
		partner_id = request.auth['partner_id']
		if partner_id is not None:
			query_set = Zone.objects.filter(partner=partner_id).all().prefetch_related('locations')
			result = [{"locations": list(item.locations.values('location_id', 'city', 'state', 'country').all()),
			           "zone_id": item.zone_id, "zone_name": item.name} for item in query_set]
			return Response({"zones": result}, status=status.HTTP_200_OK)
		else:
			return Response({"error": "partner id is incorrect"}, status=status.HTTP_400_BAD_REQUEST)

	def partial_update(self, request, pk=None):

		req_data = request.data
		req_data['partner'] = request.auth['partner_id']
		req_data['zone_id'] = pk
		serializer = ZoneLocationUpdateSerializer(data=req_data)
		serializer.is_valid(raise_exception=True)
		validated_data = serializer.validated_data
		res = serializer.update(validated_data=validated_data, instance=validated_data['instance'])
		if res:
			return Response({"message": "zone_id {} is updated with {} new location(s).".format(res.zone_id,
			                                                                                    len(res.locations.all()))},
			                status=status.HTTP_200_OK)
		else:
			return Response({"error": "partner id is incorrect"}, status=status.HTTP_400_BAD_REQUEST)
