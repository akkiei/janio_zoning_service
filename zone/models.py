from django.db import models, transaction
from partner.models import Partner
from location.models import Location


class Zone(models.Model):
	name = models.CharField(max_length=100, null=False, blank=False, unique=True)
	zone_id = models.IntegerField()
	created_at = models.DateField(auto_now_add=True, null=False)
	updated_at = models.DateField(auto_now_add=True, null=False)
	is_active = models.BooleanField(default=True)
	partner = models.ForeignKey(Partner, on_delete=models.CASCADE, null=False)
	start_date = models.DateTimeField(null=False, blank=False)
	end_date = models.DateTimeField(null=False, blank=False)
	locations = models.ManyToManyField(Location, db_column='location_id', through='ZoneLocation')
	# locations = models.ManyToManyField(Location, db_column='location_id')

	def __str__(self):
		return self.name

	def get_zone_id(self):
		with transaction.atomic():
			max_id = Zone.objects.filter().order_by('-zone_id').first()
			return max_id.zone_id + 1

	def save(self, *args, **kwargs):
		self.zone_id = self.get_zone_id()
		super(Zone, self).save(*args, **kwargs)

	def update(self, *args, **kwargs):
		super(Zone, self).save(*args, **kwargs)


class ZoneLocation(models.Model):
	id = models.AutoField(primary_key=True)
	zone = models.ForeignKey(Zone, db_column='zone_id', on_delete=models.CASCADE, unique=False)
	location = models.ForeignKey(Location, db_column='location_id', on_delete=models.CASCADE, unique=False)
	created_at = models.DateField(auto_now_add=True, null=False)


class ZoneLog(models.Model):
	created_at = models.DateField(auto_now_add=True, null=False)
	partner = models.ForeignKey(Partner, on_delete=models.CASCADE, null=False)
	zone = models.ForeignKey(Zone, db_column='zone_id', on_delete=models.CASCADE, null=False)
