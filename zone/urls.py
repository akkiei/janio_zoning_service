from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from zone.views import ZoneViewSet, ZoneLocationViewSet

router = DefaultRouter()
router.register(r'', ZoneViewSet, basename='zone')
router.register(r'zone_location', ZoneLocationViewSet, basename='zone-location')

urlpatterns = [
	path('', include(router.urls))
]
